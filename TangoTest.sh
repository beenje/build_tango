#! /usr/bin/env bash

# tangoTest build script for macOS.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

############
# MODIFY ME!
############
#
# Everything that has been set in buildrc.sh can be
# overwritten here.
#
# Customise the repository location.
export REPOSITORY=${TANGO_CONTROLS_REPO_HOST}/${MY_NAME}.git

# Customise the branch the will be checked out.
export BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG}

build_generic ${MY_NAME} remote ${REPOSITORY} ${BRANCH_OR_TAG}
end_message
)
