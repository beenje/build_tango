#! /usr/bin/env bash

# Script for macOS that installs tools to build Tango Controls on macOS.

# Make certain that coreutils are installed, because I need realpath.
brew list --versions coreutils >& /dev/null
[[ ${?} ]] || brew install coreutils

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message "Installing required packages"

for package in ${CHECK_AND_INSTALL_PACKAGES}; do
    brew_check_install ${package}
done
end_message "Installing required packages"
)
