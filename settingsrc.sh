# General settings for Tango build scripts for macOS.
#

# Check on which OS and CPU architecture we are: Is it Darwin (macOS) or Linux?
# If it is Darwin, then brew will have different installation locations.
OS=$(uname)
ARCH=$(uname -m)

# In the foreseeable future this will be the default on Apple CPUs.
export BREW_DEFAULT_ROOT_DIR="/opt/homebrew"
# If the OS is Darwin, brew is required. Unfortunately has brew different root
# directories based on the CPU architecture. Reset it when running on Intel.
[[ ${OS} = Darwin && ${ARCH} = x86_64 ]] && export BREW_DEFAULT_ROOT_DIR="/usr/local"


############
# MODIFY ME!
############
# Here you can set the path to brew's root directory.
# Usually you should leave this alone as it will just work if brew has been
# installed in the default location.
BREW_ROOT_DIR=${BREW_DEFAULT_ROOT_DIR}

# Before anything is built, it will be checked if these packages
# are available on your system.
#
# It is essential that omniorb and zeromq are installed with brew!
#
# You can chose to remove git and cmake from this list if they have
# already been installed but not with brew.
if [ ${OS} = Darwin ]; then
    export CHECK_AND_INSTALL_PACKAGES="git cmake pkg-config omniorb zeromq boost-python3 jpeg"
else
    # I am assuming, that a Linux system is not using Linux brew.
    # Therefore keep the list empty and print a reminder.
    export CHECK_AND_INSTALL_PACKAGES=""
    echo -e "Note: I hope that you have\n\tgit\n\tcmake\n\tpkg-config\n\tomniorb\n\tzeromq\n\tboost-python3\n\tjpeg\ninstalled. Otherwise this whole thing will not work."
fi

# If set to 1, then tests will automatically be run.
export RUN_TESTS=0

# Default branch or tag that will be checked out.
export TANGO_BRANCH_OR_TAG=main

# Default repository location.
TANGO_CONTROLS_REPO_HOST=https://gitlab.com/tango-controls

# Customise the version that will be appended to the cppTango installation
# directory path
export VERSION=9.4

# This directory will be created and the entire build process happens
# there.
export BUILD=tango.${VERSION}

#
# IMPORTANT!
#
# This is the installation directory for everything built here.
export INSTALLATION_DIR=${HOME}/soft/installed/${BUILD}

# Set the number of CPU cores you would like to use for the
# compilations.  The nproc command just returns the number of available
# cores.
export NCPU=$(nproc)

# Set the language standards.
export CXX_STD=14
export C_STD=11

# Set the compiler.
if [ ${OS} = Darwin ]; then
    export CC=clang
    export CXX=${CC}++
else
    # It is likely that a Linux system uses gcc.
    export CC=gcc
    export CXX=${CC//cc/++}
fi

# Generate debug symbols in the output?
# This is compiler dependent.
if [ "${CXX}" = "clang++" ]; then
    export DEBUG="-gmodules"
else
    export DEBUG="-ggdb3"
fi

# Compiler optimisation
# The debug symbols work best on clang with -O0!
export OPTIMISE="-O3"

# Set compilation and linker flags.
#
# IMPORTANT!
#
# Provide the tango include and lib dirs first.
# Up to cppTango 9.4.x it has header files that share some common header file
# names like event.h and some #includes are specified as plain system includes
# like #<event.h>. This leads to some unwanted compilation issues not only in
# cppTango, but also in pytango.

if [ ${OS} = Darwin ]; then
    export CFLAGS="${OPTIMISE} ${DEBUG} -std=c${C_STD} -I${INSTALLATION_DIR}/include/ -I${BREW_ROOT_DIR}/include/ ${CFLAGS}"
    export CXXFLAGS="${OPTIMISE} ${DEBUG} -std=c++${CXX_STD} -I${INSTALLATION_DIR}/include/ -I${BREW_ROOT_DIR}/include/ ${CXXFLAGS}"
    export LDFLAGS="-L${INSTALLATION_DIR}/lib/ -L${BREW_ROOT_DIR}/lib/ ${LDFLAGS}"
else
    export CFLAGS="${OPTIMISE} ${DEBUG} -std=c${C_STD} -I${INSTALLATION_DIR}/include/ -I/usr/local/include/ ${CFLAGS}"
    export CXXFLAGS="${OPTIMISE} ${DEBUG} -std=c++${CXX_STD} -I${INSTALLATION_DIR}/include/ -I/usr/local/include/ ${CXXFLAGS}"
    export LDFLAGS="-L${INSTALLATION_DIR}/lib64/ -L/usr/local/lib64/ ${LDFLAGS}"
fi
# Ditto but with package config files.
if [ ${OS} = Darwin ]; then
    export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${INSTALLATION_DIR}/lib/pkgconfig"
else
    export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${INSTALLATION_DIR}/lib64/pkgconfig:/usr/lib64/pkgconfig"
fi

# Set the default CMAKE options.
export DEFAULT_CMAKE_OPTIONS="-DCMAKE_C_COMPILER=${CC} -DCMAKE_CXX_COMPILER=${CXX} -DCMAKE_INSTALL_PREFIX=${INSTALLATION_DIR}"


#
# Leave this alone.
#

# Tell the OS where the libraries are installed.
if [ ${OS} = Darwin ]; then
    export DYLD_LIBRARY_PATH=${INSTALLATION_DIR}/lib:${DYLD_LIBRARY_PATH}
else
    export LD_LIBRARY_PATH=${INSTALLATION_DIR}/lib64:${LD_LIBRARY_PATH}
fi

# Make this host known as the TANGO_HOST.
export TANGO_HOST=$(hostname -f):10000

function begin_message()
{
    PACKAGE_MSG="Building and installing ${MY_NAME}"
    [[ ${#} -eq 1 ]] && PACKAGE_MSG="${1}"
    echo -e "\n\n*****\tbuild_tango: ${PACKAGE_MSG}...\n"
}

function end_message()
{
    PACKAGE="Building and installing ${MY_NAME}"
    [[ ${#} -eq 1 ]] && PACKAGE_MSG="${1}"
    echo -e "\n*****\tbuild_tango: ${PACKAGE_MSG} done.\n\n"
}

function build_generic()
{
    (
    MY_NAME=${1}
    LOCAL_OR_REMOTE=${2}
    REPOSITORY=${3}
    BRANCH_OR_TAG=${4}
    CMAKE_OPTIONS=${5}
    create_top_level_build_dir
    rsync_or_clone ${MY_NAME} ${LOCAL_OR_REMOTE} ${BRANCH_OR_TAG} ${REPOSITORY}
    [[ ! -d ${MY_NAME}/build ]] && mkdir -p ${MY_NAME}/build
    cd ${MY_NAME}/build
    cmake ${DEFAULT_CMAKE_OPTIONS} ${CMAKE_OPTIONS} ..
    cmake --build . -j ${NCPU} && cmake --install . && ( [[ ${RUN_TESTS} -eq 1 ]] && make test )
    )

}

function create_top_level_build_dir()
{
    [[ ! -d ${BUILD} ]] && mkdir -p ${BUILD}
    cd ${BUILD}
}

function rsync_or_clone()
{
    if [ "${2}" = "local" ]; then
        rsync --archive ../../${1} .
        cd ${1}
        git checkout ${3}
        cd ..
    else
        [[ -d ${1} ]] && rm -rf ${1}
        git clone --recurse-submodules --depth=1 --branch=${3} ${4}
    fi
}

# Install a package with brew if it is not already installed
function brew_check_install()
{
    package=${1}
    if [ ! -z "$(brew list --versions ${package})" ]; then
        echo "${package} is already installed.  Good."
    else
        brew install ${package}
    fi
}
