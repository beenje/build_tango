#! /usr/bin/env bash

# Build script for Tango Controls on macOS.
#

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh

. ${MY_PATH}/install_requirements.sh
. ${MY_PATH}/cppzmq.sh
. ${MY_PATH}/tango-idl.sh
. ${MY_PATH}/cppTango.sh
. ${MY_PATH}/TangoTest.sh
. ${MY_PATH}/pytango.sh
)
