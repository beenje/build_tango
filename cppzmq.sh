#! /usr/bin/env bash

# cppzmq build script for macOS.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

build_generic ${MY_NAME} remote "https://github.com/zeromq/${MY_NAME}.git" "master" "-DCPPZMQ_BUILD_TESTS=OFF"
end_message
)
