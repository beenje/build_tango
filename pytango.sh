#! /usr/bin/env bash

# pyTango build script for macOS.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

############
# MODIFY ME!
############
#
# Everything that has been set in buildrc.sh can be
# overwritten here.
#
# Customise the repository location.
#export REPOSITORY=${TANGO_CONTROLS_REPO_HOST}/${MY_NAME}.git
export REPOSITORY=https://gitlab.com/tjuerges/${MY_NAME}.git

# Customise the branch the will be checked out.
#export BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG}
#export BRANCH_OR_TAG=develop
export BRANCH_OR_TAG=develop-update_for_cppTango_main_branch_and_macOS

# The name of the Boost-Python library.  The setup.py script is not
# yet able to figure this out on macOS/Darwin.
export BOOST_PYTHON_LIB=boost_python39

function patch_tango_h()
{
    # Check if the main header file tango.h needs patching. If not yet
    # patched, patch the main header file tango.h
    [[ $(egrep -c '#include <tango_config.h>' ${INSTALLATION_DIR}/include/tango/tango.h) = "1" ]] && patch -s -p 0 ${INSTALLATION_DIR}/include/tango/tango.h ${MY_PATH}/tango.h.patch
}

function build_pytango()
{
    (
    create_top_level_build_dir
    rsync_or_clone pytango remote ${BRANCH_OR_TAG} ${REPOSITORY}
    cd pytango
    python3 setup.py build --parallel ${NCPU} && python3 setup.py install
    [[ ${RUN_TESTS} -eq 1 ]] && python3 setup.py test
    )
}

patch_tango_h
build_pytango
end_message
)
