# Build Tango Controls on macOS

## Boiler plate

### macOS

This repo is first and foremost meant to help build keyparts of Tango Controls on macOS. This is not an official Tango Controls blessed repository, nor does Tango Controls officially support macOS.

### It possibly works on Linux, too

I have used this repo to build the same keyparts of Tango Controls on my aarch64 Gentoo system. You are welcome to try it on your system, but I will not provide any support for Linux-related issues. MRs for Linux are welcome as long as they do not add functionality solely for the Linux side of these build scripts.

## Requirements

- Install `brew`!
- Then install `coreutils`. This is needed for the `realpath`utility

Everything else either comes already with macOS or will be automatically installed by the `install_requirements.sh` script. 

## The settings file `settingsrc.sh`

Modify the settings in `settingsrc.sh` that are between

```bash
 ############
 # MODIFY ME!
 ############
```

and

```bash
#
# Leave this alone.
#
```

### But I am lazy

If you are too lazy to modify the settings, please do yourself a favour and at least read what is set there. You will be surprised what is all set wrong for __your__ system. For instance you really need to modify `INSTALLATION_DIR` because I am quite certain that you dod not have the directory `~/soft/installed` on your system. You have been warned. Support requests that are caused by your lazyness will simply be closed.

## Automatically installed packages

The following packages will be installed with `brew` when you run `build.sh` or `install_requirements.txt`:

- `git`
- `cmake`
- `pkg-config`
- omniorb
- `zeromq`

You can modify this list in `settingsrc.sh`. Look for `CHECK_AND_INSTALL_PACKAGES`.

## Required manual preparation

### Python3 venv for pytango

You need a Python3 venv. Period. Create a Python3 venv where you want pytango to be installed. This will avoid messing up your system. Install `numpy` with `pip`, because `pytango` makes good use of it. While you are at it, I recommend to install and later use `ipython`, too. It will make your life much easier:

```
export MY_ENV=~/tmp/tango-env
python3 -m venv --upgrade-deps ${MY_ENV}
. ${MY_ENV}/bin/activate
python3 -m pip install numpy ipython
```

### Export the library rpath

Now you need to export a couple of environment variable:

- `DYLD_LIBRARY_PATH`: (Required) This will allow `pytango`to find `libtango.94.dylib`. Add `${INSTALLATION_DIR}/lib` to it:

    ```bash
    export DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:${INSTALLATION_DIR}/lib
    ```

- `TANGO_HOST`: (Required) This is essential. Without a `TANGO_HOST`you won't be able to enjoy much of Tango Controls. The `TANGO_HOST` runs the TangoDB, which is something like Tango Control's phone book and registry in one place. Either you run the TangoDB in a container and export port 10000 from the container or you run TangoDB elsewhere. In both cases you `export TANGO_HOST=HOSTNAME:PORT`:

    ```bash
    export TANGO_HOST=foo.bar.universe:10000
    ```

    

- `PATH`: (Optional) You can add `${INSTALLATION_DIR}/bin` to the path. This will make `TangoTest`availble without having to type in the full path to it every time you want to run it.

I have a small `alias` that takes care of all of this for me:

```bash
alias tcenv='export DYLD_LIBRARY_PATH=~/soft/installed/tango.9.4/lib:${DYLD_LIBRARY_PATH}; export PATH=${PATH}:~/soft/installed/tango.9.4/bin; export TANGO_HOST=$(hostname -f):10000; . ${MY_WORKSPACE}/tango-env/bin/activate'
```

Whenever I need to do some Tango Controls stuff, I open a terminal, execute `tcenv` and am all set.

## Build it!

Now just run

```bash
./build.sh
```

After a short while the build process should be done and you will have `cppTango`, `TangoTest`and `pytango` installed.

## Try it out

Now has come the time to try it out. I will assume that you followed my earlier recommendations and have a venv set up and installed `ipython` there.

```python
thomas@okeanos ~ 3: ipython
Python 3.9.12 (main, Mar 26 2022, 15:51:15)
Type 'copyright', 'credits' or 'license' for more information
IPython 8.2.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: import tango

In [2]: print(tango.utils.info())
PyTango 9.3.4 (9, 3, 4, 'dev', 0)
PyTango compiled with:
    Python : 3.9.12
    Numpy  : 1.22.3
    Tango  : 9.4.0
    Boost  : 1.78.0

PyTango runtime is:
    Python : 3.9.12
    Numpy  : 1.22.3
    Tango  : 9.4.0

PyTango running on:
uname_result(system='Darwin', node='okeanos', release='21.4.0', version='Darwin Kernel Version 21.4.0: Fri Mar 18 00:45:05 PDT 2022; root:xnu-8020.101.4~15/RELEASE_X86_64', machine='x86_64')
```

Enjoy!

## When things go wrong...

Well, tough luck. Good bye!

Just kidding. :wink:

### cppTango compilation

#### omniorb acts up

So far I have had only one situation where things were really weird and `cppTango` did not compile. As it turned out that was due to an older `omniorb` installation. This was the error message:

```bash
-- Build files have been written to: /Users/thomas.juerges/workspace.thomas/tango/build_tango/tango.9.4/cppTango/build
[  0%] Generate tango.h, tangoSK.cpp and tangoDynSK.cpp from idl
[  0%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/Appender.cpp.o
[  0%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/AppenderAttachable.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/LayoutAppender.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/FileAppender.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/RollingFileAppender.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/OstreamAppender.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/Layout.cpp.o
/bin/sh: /usr/local/bin/omniidl: /usr/bin/python: bad interpreter: No such file or directory
make[2]: *** [cppapi/server/idl/tango.h] Error 126
make[1]: *** [cppapi/server/idl/CMakeFiles/idl_source.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
[  2%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/PatternLayout.cpp.o
```

As you can see, in line 10 the build process bails out because of a `bad interpreter`. Freely translated, this means that the build process tried to run `/ur/bin/python`, but it could not be found. Well, naturally it cannot, because macOS Monterey (12.3.1) does not provide Python2 any more. But guess what? The error message is misleading. What really needs to be done is to reinstall `omniorg`:

```bash
brew reinstall omniorb
```

Once this has been done, the build should proceed smoothly until the end.

### Other issues?

Open an issue here. I will try to help.